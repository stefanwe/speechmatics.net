﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RestSharp;

namespace SpeechMatics
{
    public partial class SpeechMaticsClient
    {

        IRestClient restClient;

        public SpeechMaticsClient(string userId, string authToken)
        {
            restClient = new RestClient("https://api.speechmatics.com/v1.0/user/{userId}");
            restClient.AddDefaultUrlSegment("userId", userId);
            restClient.AddDefaultParameter("auth_token", authToken, ParameterType.QueryString);
        }
    }
}
