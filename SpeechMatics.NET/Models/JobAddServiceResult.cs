﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    internal class JobAddServiceResult
    {
        [DeserializeAs(Name="balance")]
        public int Balance { get; set; }
        [DeserializeAs(Name="check_wait")]
        public int CheckWait { get; set; }
        [DeserializeAs(Name="cost")]
        public int Cost { get; set; }
        [DeserializeAs(Name="id")]
        public int JobId { get; set; }
    }
}
