﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    public class User
    {
        [DeserializeAs(Name = "user.balance")]
        public int Balance { get; set; }
        [DeserializeAs(Name = "user.email")]
        public string Email { get; set; }
        [DeserializeAs(Name = "user.id")]
        public int Id { get; set; }
    }
}
