﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    internal class JobGetServiceResult
    {
        [DeserializeAs(Name="job")]
        public Job Job { get; set; }
    }
}
