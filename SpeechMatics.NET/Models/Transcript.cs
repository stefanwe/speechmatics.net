﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    public class Transcript
    {
        [DeserializeAs(Name="job.lang")]
        public string Language { get; set; }
        [DeserializeAs(Name = "job.user_id")]
        public int UserId { get; set; }
        [DeserializeAs(Name = "job.name")]
        public string Name { get; set; }
        [DeserializeAs(Name = "job.duration")]
        public int Duration { get; set; }
        [DeserializeAs(Name = "job.created_at")]
        public DateTime CreatedAt { get; set; }
        [DeserializeAs(Name = "job.id")]
        public int JobId { get; set; }
        [DeserializeAs(Name = "format")]
        public string Format { get; set; }

        [DeserializeAs(Name="speakers")]
        public List<Speaker> Speakers { get; set; }

        [DeserializeAs(Name = "words")]
        public List<Word> Words { get; set; }

    }

    public class Speaker
    {
        [DeserializeAs(Name = "duration")]
        public string Duration { get; set; }
        [DeserializeAs(Name="confidence")]
        public string Confidence { get; set; }
        [DeserializeAs(Name = "name")]
        public string Name { get; set; }
        [DeserializeAs(Name = "time")]
        public string Time { get; set; }
    }

    public class Word
    {
        [DeserializeAs(Name="duration")]
        public string Duration { get; set; }
        [DeserializeAs(Name="confidence")]
        public string Confidence { get; set; }
        [DeserializeAs(Name="name")]
        public string Name { get; set; }
        [DeserializeAs(Name="time")]
        public string Time { get; set; }
    }
}
