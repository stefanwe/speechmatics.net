﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    public partial class SpeechMaticsClient
    {
        public virtual Transcript GetTranscript(string jobId)
        {
            var request = new RestRequest("/jobs/{jobId}/transcript");
            request.AddUrlSegment("jobId", jobId);
            var result = restClient.Execute<Transcript>(request);

            return result.Data;
        }
    }
}
