﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    internal class JobListServiceResult
    {
        [DeserializeAs(Name = "jobs")]
        public List<Job> Jobs { get; set; }
    }
}
