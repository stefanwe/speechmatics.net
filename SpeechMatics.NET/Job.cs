﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    public partial class SpeechMaticsClient
    {
        public virtual IEnumerable<Job> ListJobs()
        {
            var request = new RestRequest();
            request.Resource = "/jobs";

            var result = restClient.Execute<JobListServiceResult>(request);
           
            return result.Data.Jobs;
        }

        public virtual Job GetJob(int jobId)
        {
            var request = new RestRequest("/jobs/{jobId}");
            request.AddUrlSegment("jobId", jobId.ToString());

            var result = restClient.Execute<JobGetServiceResult>(request);

            return result.Data.Job;

        }

        public virtual int AddJob(string fileName, string meta, string model, string callback)
        {
            var request = new RestRequest("/jobs/", Method.POST);

            FileInfo info = new FileInfo(fileName);
            request.AddFile("data_file", File.ReadAllBytes(fileName), info.Name);
            request.AlwaysMultipartFormData = true;
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddParameter("model", model, ParameterType.GetOrPost);
            request.AddParameter("notification", "callback", ParameterType.GetOrPost);
            request.AddParameter("callback", callback, ParameterType.GetOrPost);
            request.AddParameter("meta", meta, ParameterType.GetOrPost);

            var result = restClient.Execute<JobAddServiceResult>(request);
           return result.Data.JobId;
        }
    }
}
