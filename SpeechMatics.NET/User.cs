﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    public partial class SpeechMaticsClient
    {
        public virtual User GetAccountInformation()
        {
            var request = new RestRequest();
            var result = restClient.Execute<User>(request);
            return result.Data;
        }
    }
}
