﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMatics
{
    public class Job
    {
        [DeserializeAs(Name = "created_at")]
        public DateTime CreatedAt { get; set; }
        [DeserializeAs(Name = "duration")]
        public int Duration { get; set; }
        [DeserializeAs(Name = "id")]
        public int Id { get; set; }
        [DeserializeAs(Name = "job_status")]
        public string JobStatus { get; set; }
        [DeserializeAs(Name = "job_type")]
        public string JobType { get; set; }
        [DeserializeAs(Name = "lang")]
        public string Language { get; set; }
        [DeserializeAs(Name = "meta")]
        public string Metadata { get; set; }
        [DeserializeAs(Name = "name")]
        public string Name { get; set; }
        [DeserializeAs(Name = "next_check")]
        public int NextCheck { get; set; }
        [DeserializeAs(Name = "transcription")]
        public string Transcription { get; set; }
        [DeserializeAs(Name = "text_name")]
        public string TextName { get; set; }
        [DeserializeAs(Name = "url")]
        public string Url { get; set; }
        [DeserializeAs(Name = "user_id")]
        public int UserId { get; set; }
        [DeserializeAs(Name = "alignment")]
        public string Alignment { get; set; }
        [DeserializeAs(Name = "check_wait")]
        public int CheckWait { get; set; }
        [DeserializeAs(Name = "notification")]
        public string Notification { get; set; }

    }
}
